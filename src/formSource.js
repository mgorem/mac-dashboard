export const companyInputs = [
  {
    id: "username",
    label: "Username",
    type: "text",
    placeholder: "company_x",
  },
  {
    id: "companyName",
    label: "Registered Company Name",
    type: "text",
    placeholder: "Company Name",
  },
  {
    id: "email",
    label: "Email",
    type: "mail",
    placeholder: "companyname@gmail.com",
  },
  {
    id: "phone",
    label: "Phone",
    type: "text",
    placeholder: "+254 123 456 789",
  },
  {
    id: "password",
    label: "Password",
    type: "password",
  },
  {
    id: "address",
    label: "Address",
    type: "text",
    placeholder: "Westlands, 12, Nairobi",
  },
  {
    id: "county",
    label: "County",
    type: "text",
    placeholder: "Nairobi",
  },
  {
    id: "registrationDate",
    label: "Date of Registration",
    type: "text",
    placeholder: "Registration Date",
  },
];

export const adminInputs = [
  {
    id: 1,
    label: "Username",
    type: "text",
    placeholder: "john_doe",
  },
  {
    id: 2,
    label: "Name and Surname",
    type: "text",
    placeholder: "Your Name",
  },
  {
    id: 3,
    label: "Email",
    type: "mail",
    placeholder: "adminname@gmail.com",
  },
  {
    id: 4,
    label: "Phone",
    type: "text",
    placeholder: "+254 123 456 789",
  },
  {
    id: 5,
    label: "Password",
    type: "password",
  },
  {
    id: 6,
    label: "Address",
    type: "text",
    placeholder: "Gigiri, 12, Nairobi",
  },
  {
    id: 7,
    label: "County",
    type: "text",
    placeholder: "Nairobi",
  },
];
