import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);
  const [adminData, setAdminData] = React.useState({});

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleInput = (e) => {
    const id = e.target.id;
    const value = e.target.value;

    setAdminData({...adminData, [id]: value});
  }
  console.log(adminData);

  const handleAdminDetails = (e) => {
    e.preventDefault();
    
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Add Company Admin
      </Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Company Admin Details</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Enter details of the company admin here
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            onChange={handleInput}
            fullWidth
            variant="standard"
          />
        <TextField
            autoFocus
            margin="dense"
            id="name"
            label="Full Name"
            type="text"
            onChange={handleInput}
            fullWidth
            variant="standard"
          />
        <TextField
            autoFocus
            margin="dense"
            id="phone"
            label="Phone Number"
            type="phone"
            onChange={handleInput}
            fullWidth
            variant="standard"
          />
        <TextField
            autoFocus
            margin="dense"
            id="company"
            label="Company"
            type="text"
            onChange={handleInput}
            fullWidth
            variant="standard"
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
          <Button onClick={handleAdminDetails}>ADD</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}